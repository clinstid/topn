package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	inputPtr := flag.String("i", "input.txt", "Path to input file")
	numCountPtr := flag.Int("n", 10, "Count of top numbers to track")
	flag.Parse()

	fmt.Println("Input file is:", *inputPtr)
	fmt.Println("Tracking top", *numCountPtr, "numbers")

	file, err := os.Open(*inputPtr)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	topN := getNumbers(scanner, *numCountPtr)

	// Walk backwards through the slice printing out the numbers.
	for idx := len(topN) - 1; idx >= 0; idx -= 1 {
		fmt.Println(topN[idx])
	}
}
