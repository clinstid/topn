package main

import (
	"bufio"
	"math"
	"strconv"
	"strings"
	"testing"
)

// genNumSequence generates a number sequence from first to last.
func genNumSequence(first, last int) []int {
	count := int(math.Abs(float64(first-last)) + 1)
	seq := make([]int, count)
	if first <= last {
		for i := first; i <= last; i++ {
			seq[i-first] = i
		}
	} else {
		for i := first; i >= last; i-- {
			seq[first-i] = i
		}
	}
	return seq
}

// numSeqScanner creates a bufio.Scanner out of a slice of integers to aid in
// testing.
func numSeqScanner(seq []int) *bufio.Scanner {
	var str string
	count := len(seq)
	for i := range seq {
		str += strconv.Itoa(seq[i])
		if i < count-1 {
			str += "\n"
		}
	}
	return bufio.NewScanner(strings.NewReader(str))
}

// sliceEq compares two slices of integers and returns true if they are equal.
func sliceEq(l, r []int) bool {
	count := len(l)
	if count != len(r) {
		return false
	}

	for i := 0; i < count; i++ {
		if l[i] != r[i] {
			return false
		}
	}
	return true
}

// TestGetNumbers test the getNumbers function.
func TestGetNumbers(t *testing.T) {
	// Basic test cases, incrementing and decrementing values
	basicInc := genNumSequence(-10, 10)
	basicDec := genNumSequence(10, -10)
	basicN := 10
	basicExpected := genNumSequence(1, 10)

	// Small input not sorted
	smallInput := []int{10, 40, 20}
	smallExpected := []int{10, 20, 40}

	// Duplicate values, not sorted
	dupInput := []int{2, 2, 3, 3, 4, 4, 1, 1}
	dupExpected := []int{1, 2, 3, 4}

	testCases := []struct {
		tc             string
		n              int
		input          []int
		expectedOutput []int
	}{
		{
			tc:             "-10 to 10 with n=10 should produce 1 through 10",
			n:              basicN,
			input:          basicInc,
			expectedOutput: basicExpected,
		},
		{
			tc:             "10 to -10 with n=10 should produce 1 through 10",
			n:              basicN,
			input:          basicDec,
			expectedOutput: basicExpected,
		},
		{
			tc:             "Sequence with len < n should just sort the input",
			n:              basicN,
			input:          smallInput,
			expectedOutput: smallExpected,
		},
		{
			tc:             "Sequence with duplicate values should sort the input and eliminate duplicates",
			n:              basicN,
			input:          dupInput,
			expectedOutput: dupExpected,
		},
	}

	for _, test := range testCases {
		t.Run(test.tc, func(t *testing.T) {
			topn := getNumbers(numSeqScanner(test.input), test.n)
			if !sliceEq(topn, test.expectedOutput) {
				t.Errorf("Output %v does not match expected output %v", topn, test.expectedOutput)
			}
		})
	}
}
