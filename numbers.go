package main

import (
	"bufio"
	"log"
	"sort"
	"strconv"
)

func insertOrAppend(slice []int, index, value int) []int {
	if index == len(slice) {
		return append(slice, value)
	}

	// The following appends the portion of the slice before the index, the
	// value to insert at the index, and then the rest of the slice.
	//
	// The ellipsis unpacks the elements of the slices (returned by the second
	// append and the second portion of the slice) so they can be passed into
	// the append function.
	return append(slice[:index], append([]int{value}, slice[index:]...)...)
}

// getNumbers scans through lines where each line contains a number and returns
// a slice of the largest numCount numbers found in ascending order.
func getNumbers(scanner *bufio.Scanner, numCount int) []int {
	topN := []int{}
	var count int
	for scanner.Scan() {
		count += 1
		n, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}

		// First value goes into the slice
		if len(topN) == 0 {
			topN = append(topN, n)
			continue
		}

		// We're keeping the slice sorted, so if the value is less than the
		// first value in the slice and we already have enough numbers, then skip it.
		if n < topN[0] && len(topN) >= numCount {
			continue
		}

		// Find where the value would be inserted into the slice. SearchInts
		// does a binary search so it should be relatively efficient.
		insertIdx := sort.SearchInts(topN, n)

		// The value at the returned index may be the same as the value we're
		// checking, so we don't bother inserting if it's already the correct
		// value.
		if insertIdx == len(topN) || topN[insertIdx] != n {
			topN = insertOrAppend(topN, insertIdx, n)
		}

		// Remove the smallest number if we're over the count of numbers we
		// need.
		if len(topN) > numCount {
			topN = topN[len(topN)-numCount:]
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return topN
}
